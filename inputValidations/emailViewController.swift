//
//  emailViewController.swift
//  inputValidations
//
//  Created by Taslima Roya on 6/7/20.
//  Copyright © 2020 Taslima Roya. All rights reserved.
//

import UIKit
 
class emailViewController: UIViewController {

    @IBOutlet weak var emailValidation: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

  
    @IBAction func emailActionButton(_ sender: UIButton) {
        
        if (emailValidation.text?.isValidEmail)! {
                   print("email is valid")
               } else{
                   print("email is not valid")
               }
    }
    
}

extension String{
    
    var isValidEmail:Bool{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
             
             let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
             return emailTest.evaluate(with: self)
    }
}
    
    
  

